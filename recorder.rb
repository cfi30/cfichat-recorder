#!/usr/bin/env ruby
require 'bundler/setup'
require 'cfichat/client'
require 'digest/sha1'
require 'sqlite3'
require 'yaml'

Settings = YAML.load_file 'settings.yml'

DB = SQLite3::Database.new 'recorder.db'
DB.busy_timeout = 100

DB.execute <<-SQL
CREATE TABLE IF NOT EXISTS users (
  nick TEXT UNIQUE COLLATE nocase,
  is_client INTEGER
)
SQL

DB.execute <<-SQL
CREATE TABLE IF NOT EXISTS queue (
  id INTEGER PRIMARY KEY,
  nick TEXT,
  author TEXT,
  color TEXT,
  message TEXT,
  time DATETIME,
  is_private INTEGER
)
SQL

class Recorder < CfiChat::Client::Bot
  DATETIME_FORMAT = '%F %T'.freeze

  Strings = {
    already_enabled: "j'ai déjà une cassette à ton nom",
    confirm_plural: 'je vais leur dire quand ils reviendront',
    confirm_singular: 'je vais lui dire quand il reviendra',
    default: '(par défaut)',
    enable_success: "j'ai créé une nouvelle cassette pour toi",
    msg_disabled: "je ne trouve pas la cassete de %s",
    msg_online: "%s est connecté, dis-lui toi même ! :langue:",
    playback: '%s> [color=%s][%s @ %s]: %s[/color]',
    status: "Au service de %d utilisateurs depuis le %s. J'ai %d message(s) dans mes cassettes.",
    stop_404: "je n'ai pas de cassette à ton nom",
    stop_success: "j'ai détruit ta cassette",
  }.freeze

  # ensure the strings cannot be modified
  Strings.each {|str| str.freeze }

  def initialize(client)
    super

    @client = client

    EM.add_periodic_timer Settings[:timer], &method(:on_timeout)
    @client.add_message_hook &method(:on_message)

    @start_time = DateTime.now
  end

  def cmd_man(*args)
    def_key = Settings[:auto_record] ? :rec : :stp
    default = {def_key => Strings[:default]}

    yield <<-MAN
    [quote=Manuel]
    [b]man[/b]: affiche ce manuel
    [b]msg NICK *MESSAGE[/b]: enregistre un message manuellement
    [b]record[/b]: conserve les messages manqués #{default[:rec]}
    [b]status[/b]: affiche l'état du bot
    [b]stop[/b]: ignore les messages manqués #{default[:stp]}
    MAN
  end
  alias :cmd_help :cmd_man

  def cmd_status(*args)
    yield Strings[:status] % [clients_count, format_time(@start_time), queue_size]
  end

  def cmd_record(*args)
    if enabled? @message.author
      yield Strings[:already_enabled]
    else
      enable @message.author
      yield Strings[:enable_success]
    end
  end

  def cmd_stop(*args)
    if enabled? @message.author
      disable @message.author
      yield Strings[:stop_success]
    else
      yield Strings[:stop_404]
    end
  end

  def cmd_msg(*args)
    nick, *message = args
    nick = real nick

    if @client.user_list.find nick
      yield Strings[:msg_online] % nick
      return
    end

    unless enabled? nick
      yield Strings[:msg_disabled] % nick
      return
    end

    fake_msg = @message.clone
    fake_msg.body[0..-1] = message.join "\x20"

    record nick, fake_msg
    yield Strings[:confirm_singular]
  end

  private
  def format_time(datetime)
    datetime.strftime DATETIME_FORMAT
  end

  def on_timeout
    me = @client.user_list.find Settings[:username]

    if me && me.sleeping?
      @client.speak '/wake'
    end

    DB.execute 'SELECT id, nick, author, color, message, time, is_private FROM queue' do |row|
      id, nick, author, color, message, time, is_private = row

      next unless @client.user_list.find nick

      prefix = is_private == 1 ? '/pm ' : ''

      message = Strings[:playback] % [nick, color, author, time, message]
      message.sub! /(\[(?:code|quote)(?:=[^\]]+)?\].+)\[\/color\]\Z/m, '[/color]\1'

      @client.speak prefix + message

      puts "played #{row.inspect}"

      DB.execute 'DELETE FROM queue WHERE id = ?', id
    end
  end

  def on_message(msg)
    return if !msg.author || msg.author == Settings[:username]

    meet msg.author

    count = 0

    msg.receivers.each {|nick|
      nick = real nick

      next if @client.user_list.find nick
      next unless enabled? nick

      record nick, msg

      count += 1
    }

    if count > 0
      message = count == 1 \
        ? Strings[:confirm_singular]
        : Strings[:confirm_plural]

      @client.speak "/pm #{msg.author}> #{message}"
    end
  end

  def knows?(nick)
    row = DB.get_first_row 'SELECT 0 FROM users WHERE nick = ?', nick
    not row.nil?
  end

  def enabled?(nick)
    row = DB.get_first_row 'SELECT 0 FROM users WHERE nick = ? AND is_client = 1', nick
    not row.nil?
  end

  def meet(nick)
    return if knows? nick

    DB.execute \
      'INSERT INTO users (nick, is_client) VALUES (?, ?)',
      [nick, Settings[:auto_record] ? 1 : 0]
  end

  def enable(nick)
    meet nick

    DB.execute 'UPDATE users SET is_client = 1 WHERE nick = ?', nick
  end

  def disable(nick)
    meet nick

    DB.execute 'UPDATE users SET is_client = 0 WHERE nick = ?', nick
  end

  def record(nick, msg)
    time = DateTime.strptime msg.time, Settings[:server_time_format]

    DB.execute \
      'INSERT INTO queue (nick, author, color, message, time, is_private) values (?, ?, ?, ?, ?, ?)',
      [nick, msg.author, msg.color, msg.body, format_time(time), msg.private? ? 1 : 0]

    puts "recorded #{msg.to_s}"
  end

  def clients_count
    count = DB.get_first_row 'SELECT COUNT(nick) FROM users WHERE is_client = 1'
    count.first
  end

  def queue_size
    count = DB.get_first_row 'SELECT COUNT(id) FROM queue'
    count.first
  end

  def real(nick)
    row = DB.get_first_row 'SELECT nick FROM users WHERE nick = ? LIMIT 1', nick
    row ? row.first : nick
  end
end


client = CfiChat::Client.new Settings[:server]
hashed_pass = Digest::SHA1.hexdigest Settings[:password]
client.cookies = "username=#{Settings[:username]}; password=#{hashed_pass}"

loop do
  client.run Settings[:login_room] do
    Recorder.new client
  end

  puts
  puts "Reconnecting in 30 seconds..."

  # automatic reconnection after a kick or any other fatal server error
  sleep 30
end
